#!/usr/bin/python

# this short script selects a random jpg image in www/strips and
# serves it to the requester.  It's careful to warn the browser
# not to cache the image, which helps ensure that the reader
# gets a different image on every page.
#
#                      C. Scott Ananian, cananian@alum.mit.edu, 17 June 2006

import os
import random
import datetime
import Cookie
TSI = "TechSquaresImage" # the name of the cookie used by this script.
C = Cookie.SimpleCookie()
C.load(os.environ.get("HTTP_COOKIE", ""))
# if cookie is set, set cookie to cookie + 1; else set cookie to 0
override=None
try:
    override = int(C[TSI].value)
    C[TSI] = 1 + override
except KeyError:
    C[TSI] = 0
#print C.output()+"\r\n", # comment this out to disable cookies

basepath="/mit/tech-squares/www/strips/"
now = datetime.datetime.utcnow().strftime("%A, %d-%b-%Y %T GMT")

print "Content-type: image/jpeg\r\n",
print "Title: Random Squares Logo\r\n",
print "Cache-Control: no-cache, must-revalidate\r\n",
print "Pragma: no-cache\r\n",
print "Expires: "+now+"\r\n",
print "\r\n",


files = [f for f in os.listdir(basepath) if f.endswith(".jpg")]
which = random.choice(files)

# if cookie was set, then override choice of 'which'
# with cookie value.  This ensures a linear progression through the
# images, which is handy for testing.
if override is not None:
    which = files[abs(override) % len(files)]

#which = files[0] # alternative mechanism for testing

f = open(basepath + which, 'r')
data = f.read()
f.close()
print data,
